#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
//#include <opencv2/video.hpp>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>

#define MIN_RECORD_TIME 15
#define TELEGRAM_WAIT_TIME 30

using namespace cv;

//Umbral de sensibilidad para la deteccion de movimiento
//#define UMBRAL 80   // <<<Este funciona excelente
#define UMBRAL 80 // <<<Umbral de prueba

//Cuanto tiempo se grabara de video en segundos
#define RECORD_TIME 30
#define THREADS 4

typedef struct _Tiempo
{
    int year;
    int month;
    int day;
    int hora;
    int minuto;
    int segundo;
    
}Tiempo;

typedef struct _uchar4
{
	unsigned char x;
	unsigned char y;
	unsigned char z;
	unsigned char w;
}uchar4;

typedef struct recordTime
{
    time_t start;
    time_t current;
	time_t end;
	time_t nextAvail;
    unsigned char recording;
}RecordTime;


RecordTime startTiming( int recordingTime )
{
    RecordTime tiempo;
    tiempo.start = time(NULL);
    tiempo.current = time(NULL);
	tiempo.end = tiempo.start + recordingTime;
	tiempo.nextAvail = tiempo.start;
    return tiempo;
}


//Funcion que devuelve la estructura tiempo con el tiempo actual
Tiempo getTiempo()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    //Conseguimos el tiempo actual y lo asignamos a la estructura que vamos a devolver
    //esto facilita su lectura
    Tiempo miTiempo;
    miTiempo.year = tm.tm_year + 1900;
    miTiempo.month = tm.tm_mon + 1;
    miTiempo.day = tm.tm_mday;
    miTiempo.hora = tm.tm_hour;
    miTiempo.minuto = tm.tm_min;
    miTiempo.segundo = tm.tm_sec;
    return miTiempo;
}

int createFile( char *filePath, char *string )
{
	// create a FILE typed pointer
	FILE *filePtr; 
		
	//Open the file 
	filePtr = fopen( filePath, "w"); 

	//Write to the file
	fprintf( filePtr, string );

	// Close the file
	fclose(filePtr);

	return 0;
}

void freeBuffer( uchar4 *buffer[], int bufferSize );


//Structura que contendra la informacion de la captura de video
typedef struct VideoMovementRecord
{
	char outputName[256];
	int codec;
	Size size;
	float fps;
	char outputFolder[1024];
	bool closed;
} *VideoMovementProperties;


VideoWriter mainVideo, teleVideo;

//Funcion que verifica si existe folder, en caso
//contrario, crea folder nuevo con fecha
int tryOpen( const char *folder )
{
    DIR* dir = opendir( folder );

    // Directory exists
    if (dir)
    {
        closedir(dir);
        return 1;
    }

    // Directory does not exist
    else if (ENOENT == errno)
    {
        mkdir(folder, 0700);
        return 1;
    }
    //opendir() failed for some other reason
    else
    {
        return 0;
    }
}
//Funcion que crea los strings a las carpetas de salida, si no
//existen las carpetas entonces las crea
int getOutputVideoName( char *output, const char *rootFolder, const char *codec )
{
    Tiempo elTiempo=getTiempo();
    char buffer[256];
    char miniBuffer[50];

    if( tryOpen(rootFolder) )
    {
        //Checar primero root
        strcpy(buffer, rootFolder);

        if( rootFolder[strlen(rootFolder)-1] != '/' )
        {
            strcat( buffer, "/" );
        }

        //Convertimos a string el anio
        sprintf( miniBuffer, "%d/", elTiempo.year );
        strcat(buffer, miniBuffer);

        //Checar si existe el folder del año
        if( ! tryOpen(buffer) ) return 5;
        
        //Convertimos a string el anio
        sprintf( miniBuffer, "%d/", elTiempo.month );
        strcat( buffer, miniBuffer );

        //Checar si existe el folder del mes
        if( ! tryOpen(buffer) ) return 6;
        
        //Convertimos a string el anio
        sprintf( miniBuffer, "%d/", elTiempo.day );
        strcat( buffer, miniBuffer );

        //Checar si existe el folder del dia
        if( ! tryOpen(buffer) ) return 7;
        
        //Creamos string de salida
        sprintf( output, "%s%d_%d_%d.%s", buffer, elTiempo.hora, elTiempo.minuto, elTiempo.segundo, codec  );

        //No hubo errors
        return 0;
    }
    else
    {
        return 4;
    }
}

//Funcion que crea los strings a las carpetas de salida, si no
//existen las carpetas entonces las crea
int getOutputTelegramNot( char *output, const char *rootFolder )
{
    Tiempo elTiempo=getTiempo();
    char buffer[256];
    char miniBuffer[50];
        //Checar primero root
        strcpy(buffer, rootFolder);

        if( rootFolder[strlen(rootFolder)-1] != '/' )
        {
            strcat( buffer, "/" );
        }

        //Convertimos a string el anio
        sprintf( miniBuffer, "%d_", elTiempo.year );
        strcat(buffer, miniBuffer);
        
        //Convertimos a string el anio
        sprintf( miniBuffer, "%d_", elTiempo.month );
        strcat( buffer, miniBuffer );

        //Convertimos a string el anio
        sprintf( miniBuffer, "%d_", elTiempo.day );
		strcat( buffer, miniBuffer );
		
        //Creamos string de salida
        sprintf( output, "%s%d_%d_%d", buffer, elTiempo.hora, elTiempo.minuto, elTiempo.segundo  );

        //No hubo errors
        return 0;

}


//Corazon del programa
int smartRecord( Mat image, VideoMovementProperties teleVideoProp, VideoMovementProperties fullVideo, bool movement, RecordTime *mainVideoTime, RecordTime *sTimeTelegram  ) //Tiene que recibir despues ( VideoWritter,  Mat, Tiempo )
{
	//Si hay movimiento
    if( movement == true )
    {
		//Videowrite, write en archivo
		if( fullVideo->closed && mainVideoTime->nextAvail < time(NULL) )
		{
			//crear string de video de salida
			if ( getOutputVideoName( fullVideo->outputName, "/tmp/VVI_Tahcnikuetzin", "avi" ) != 0 )
			{
				printf("Error creando string de video de salida: %s\n", fullVideo->outputName );
				return 8;
			}


			//printf("Grabando nuevo video: %s\n", fullVideo->outputName);
			popen("/opt/raspiVVI/VVI/AdminNotifications.sh 'Movement detected! Now recording...'", "r");

			//Intentar abrir video 
			mainVideo.open( fullVideo->outputName, CV_FOURCC('M','J','P','G'), fullVideo->fps, fullVideo->size, true );

			//Si no se pudo abrir  el archivo, entonces error
			if ( !mainVideo.isOpened() )
			{
				printf("No se pudo crear video de salida Main");
				return 3;
			}

			//Escribir primera imagen en el archivo
			mainVideo.write(image);
			fullVideo->closed=false;
			*mainVideoTime = startTiming(60*2);
			mainVideoTime->nextAvail = time(NULL) + 60*2;
		}

		if( teleVideoProp->closed && sTimeTelegram->nextAvail < time(NULL) )
		{
			//crear string de video de salida
			if ( getOutputVideoName( teleVideoProp->outputName, "/tmp/VVI_Tahcnikuetzin", "Telegram.avi" ) != 0 )
			{
				printf("Error creando string de video de salida: %s\n", teleVideoProp->outputName );
				return 8;
			}
			teleVideo.open( teleVideoProp->outputName, CV_FOURCC('M','J','P','G'), teleVideoProp->fps, teleVideoProp->size, true );

			//Si no se pudo abrir  el archivo, entonces error
			if ( !teleVideo.isOpened() )
			{
				printf("No se pudo crear video de salida Tele");
				return 3;
			}

			teleVideo.write(image);
			teleVideoProp->closed=false;
			*sTimeTelegram = startTiming(TELEGRAM_WAIT_TIME*2);
			sTimeTelegram->nextAvail = time(NULL) + TELEGRAM_WAIT_TIME * 2;

		}

		if( !teleVideoProp->closed && sTimeTelegram->end < time(NULL) )
		{
			//Escribir imagen en video
			teleVideo.write(image);
			teleVideo.release();

			//Liberar candado
			teleVideoProp->closed = true;

			//Podra grabar despues de n segundos
			sTimeTelegram->nextAvail = time(NULL)+TELEGRAM_WAIT_TIME*2;

			//Informar al servicio de una deteccion nueva
			//Aqui hacer un fichero o escribir en un archivo
			char output[256];
			getOutputTelegramNot( output, "/etc/raspiVVI/VVI/new/" );
			createFile( output, teleVideoProp->outputName );
			//printf("Se cerro el archivo Telegram\n");
		}

		//else
		//printf("Hay movimiento pero no pudo grabar\n");
	}
	else
	{
		if( !teleVideoProp->closed && sTimeTelegram->nextAvail < time(NULL) )
		{
			//Escribir imagen en video
			teleVideo.write(image);
			teleVideo.release();

			//Liberar candado
			teleVideoProp->closed = true;

			//Podra grabar despues de n segundos
			sTimeTelegram->nextAvail = time(NULL)+TELEGRAM_WAIT_TIME*2;

			//Informar al servicio de una deteccion nueva
			//Aqui hacer un fichero o escribir en un archivo
			char output[256];
			getOutputTelegramNot( output, "/etc/raspiVVI/VVI/new/" );
			createFile( output, teleVideoProp->outputName );
			//printf("Se cerro el archivo Telegram\n");
		}

		if( !fullVideo->closed && mainVideoTime->nextAvail < time(NULL) )
		{
			//Escribir imagen en video
			mainVideo.write(image);
			mainVideo.release();

			//Liberar candado
			fullVideo->closed = true;

			//Podra grabar despues de n segundos
			mainVideoTime->nextAvail = time(NULL) + MIN_RECORD_TIME;
			//Informar al servicio de una deteccion nueva
			//Aqui hacer un fichero o escribir en un archivo

			//printf("Se cerro el archivo\n");
		}
	}

	if( !fullVideo->closed )
	{
		//Escribir imagen en video
		mainVideo.write(image);
	}

	if( !teleVideoProp->closed )
	{
		//Escribir imagen en video
		teleVideo.write(image);
	}

    return 0;
}


//Procedimiento para imprimir pixel en posicion especifica pero de todo el buffer
void printPixelinBufferPos( uchar4 *buffer[], int bufferSize, int pixel)
{
	int i;
	for( i=0; i<bufferSize; ++i )
	{
		printf("%d ", buffer[i][pixel].x);
	}
	printf("\n");
}
//float getPixelChangePercentage
//Funcion que devuelve el valor promedio de Rojo de todo el buffer
unsigned char promedioPixelXBuffer( uchar4 *buffer[], int bufferSize, int pixel)
{
	int i;
	float sum=0.0;
	for( i=0; i<bufferSize; ++i )
	{
		sum += (float) buffer[i][pixel].x;
	}

	return ((unsigned char)(sum/(float)bufferSize));
}

//Funcion que devuelve el valor promedio de Verde de todo el buffer
unsigned char promedioPixelYBuffer( uchar4 *buffer[], int bufferSize, int pixel)
{
	int i;
	float sum=0.0;
	for( i=0; i<bufferSize; ++i )
	{
		sum += (float) buffer[i][pixel].y;
	}

	return ((unsigned char)(sum/(float)bufferSize));
}

//Funcion que devuelve el valor promedio de Azul de todo el buffer
unsigned char promedioPixelZBuffer( uchar4 *buffer[], int bufferSize, int pixel)
{
	int i;
	float sum=0.0;
	for( i=0; i<bufferSize; ++i )
	{
		sum += (float) buffer[i][pixel].z;
	}

	return ((unsigned char)(sum/(float)bufferSize));
}

//Crea el vector con el fondo promedio y listo para aplicarse la formula
void createBackground( uchar4 *background, int size, uchar4 *buffer[], int bufferSize )
{
	int i;
	for( i=0; i<size; ++i )
	{
		background[i].x=promedioPixelXBuffer( buffer, bufferSize, i);
		background[i].y=promedioPixelYBuffer( buffer, bufferSize, i);
		background[i].z=promedioPixelZBuffer( buffer, bufferSize, i);
		background[i].w=255;
	}

}

//Se aplica la formula Canchola del fondo perpetuo
unsigned char applyBackgroundFormula( unsigned char fondo, unsigned char k, unsigned char newPixel )
{
	return  (unsigned char)(  (  ( (int)fondo * ((int)k-1) ) + (int)newPixel ) / (int)k  );
}

//Funcion que actualiza el fondo promedio dada una imagen nueva
void updateBackground( uchar4 *background, uchar4 *imageNow, int size, int bufferSize )
{
	int i;
	for( i=0; i<size; ++i )
	{
		background[i].x=applyBackgroundFormula( background[i].x, bufferSize, imageNow[i].x );
		background[i].y=applyBackgroundFormula( background[i].y, bufferSize, imageNow[i].y );
		background[i].z=applyBackgroundFormula( background[i].z, bufferSize, imageNow[i].z );
	}
}




//Crear imagen de salida cambiando los sentidos de color
void createOutImage(const char *out, uchar4 *image, int rows, int cols)
{
	Mat salida1;
	Mat temp1(rows, cols, CV_8UC4, image );
	cvtColor(temp1, salida1, CV_RGBA2BGR);
	imwrite(out, salida1);
}

VideoMovementProperties createVideoObject( Size size )
{
	VideoMovementProperties videoObject = (struct VideoMovementRecord*)malloc(sizeof(struct VideoMovementRecord));

	//--- INITIALIZE VIDEOWRITER
	videoObject-> fps = 14.0;
	videoObject-> size = size;
	videoObject-> codec = CV_FOURCC( 'X', '2', '6', '4' );
	videoObject-> closed = true;
	return videoObject;

}

unsigned int movementDetect( uchar4 *webCamImage, uchar4 *fondo, uchar4 *outColored, unsigned char *estela, unsigned char currentEstela, unsigned char *estelaFinal, unsigned char umbral, unsigned char bufferSize, int rows, int cols )
{
	unsigned int changedPixels=0;

	#pragma omp parallel for num_threads(THREADS)
	for( int i=0; i<rows*cols; ++i )
	{
		int index = i;

		//Conseguimos la diferencia de las imagenes, fondo vs presente
		unsigned char red = abs( webCamImage[index].x - fondo[index].x );
		unsigned char green = abs( webCamImage[index].y - fondo[index].y );
		unsigned char blue = abs( webCamImage[index].z - fondo[index].z );

		//Verificar si pasa el umbral, si lo pasa entonces encontro movimiento
		if( red > umbral && green > umbral && blue > umbral )
		{
			//Poner pixel en blanco, que significa true
			estela[ (rows*cols*currentEstela) + index ] = 255;
			estelaFinal[ index ] = 255;
			outColored[index].x=255;
			#pragma omp atomic
			changedPixels+=1;
		}
		else outColored[index].x=webCamImage[index].x;
		outColored[index].y=webCamImage[index].y;
		outColored[index].z=webCamImage[index].z;
		
		//Actualizar el fondo
		fondo[index].x=applyBackgroundFormula( fondo[index].x, bufferSize, webCamImage[index].x );
		fondo[index].x=applyBackgroundFormula( fondo[index].x, bufferSize, webCamImage[index].x );
		fondo[index].x=applyBackgroundFormula( fondo[index].x, bufferSize, webCamImage[index].x );
	}
	return changedPixels;
}

void colorearMovement( uchar4 *webCamImage, uchar4 *outColored, unsigned char *estela, unsigned char numEstelas, int rows, int cols )
{
	int i;
	int imageSize=rows*cols;
	#pragma omp parallel for num_threads(THREADS)
	for( i=0; i<numEstelas*imageSize; ++i )
	{
		estela[i] == 255 ? outColored[i%imageSize].x=255:outColored[i%imageSize].x=webCamImage[i%imageSize].x;
		outColored[i%imageSize].y=webCamImage[i%imageSize].y;
		outColored[i%imageSize].z=webCamImage[i%imageSize].z;
	}
}

void freeBuffer(uchar4 *buffer[], int bufferSize )
{
	int i;
	for( i=0; i<bufferSize; ++i ) free(buffer[i]);
}

/*
////////////////////////////////////////////////////////////////////////////////////
	PROGRAMA PRINCIPAL
////////////////////////////////////////////////////////////////////////////////////
*/

int main()
{

	namedWindow("Original",1);
	namedWindow("Estela",1);
	namedWindow("Coloreada",1);

	//Agarrar la camara 0, podemos agarrar la camara 1,2,3,n... Depende de cuantas webcam estan conectadas
	VideoCapture cap(0);

	//No se pudo abrir webcam
	if(!cap.isOpened())
	{
		printf("No se pudo abrir webcam\n");
	return 3;
	}

	//Tamano del buffer
	int bufferSize=60;

	//Declaramos tamano del buffer
	uchar4 *buffer[bufferSize];

	int contadorBuffer=0;
	Mat frame, colorFrame;

	//Frame de la webcam
	cap >> frame;

	//Los renglones y columnas de la webcam
	int cols=frame.cols;
	int rows=frame.rows;
	
	
	//Objete que grabara los videos
	VideoMovementProperties VideoVigilancia = createVideoObject( frame.size() );
	VideoMovementProperties TeleVideo = createVideoObject( frame.size() );

	//Reservar memoria para el buffer
	int i=0;
	for( i=0; i<bufferSize; ++i )
	{
		buffer[i]=(uchar4*)malloc( sizeof(uchar4) * cols*rows );
	}

	//Fondo de la imagen
	uchar4 *background=(uchar4 *)malloc( sizeof(uchar4) * cols*rows );

	i=0;
	while( i < bufferSize )
	{
		//Frame de la webcam
		cap >> frame;

		//Convertir  frame e insertar en el buffer
		cvtColor( frame, colorFrame, CV_BGR2RGBA );

		//Tomando los apuntadores a los datos del buffer
		uchar4 *tmp= (uchar4 *)colorFrame.ptr<unsigned char>(0);

		//Copiando el buffer
		memcpy( buffer[contadorBuffer], tmp, sizeof(uchar4) * cols * rows  );
		//copyUchar4( buffer[contadorBuffer], tmp, cols*rows );

		++contadorBuffer;
		++i;
	}

	createBackground( background, cols*rows, buffer, bufferSize );
	freeBuffer( buffer, bufferSize );

	//Configuracion de la estela de seguimiento
	int numeroEstelas=2;

	//1.- Apuntadores a GPU
	uchar4 *out=NULL;

	//Para hacer la estela de luz
	unsigned char *estela=NULL, *EstelaSuperFinal=NULL;
	unsigned char currentEstela=0;
	//Conseguir el numero de pixeles que cambiaron
	unsigned int *changedPixels=NULL;

	//2.- Reservar memoria para la manipulacion de la imagen
	estela=(unsigned char *)(unsigned char *)malloc( sizeof(unsigned char) * cols * rows * numeroEstelas );
	out=(uchar4 *)malloc( sizeof(uchar4) * cols * rows );
	changedPixels=(unsigned int *)malloc(sizeof(unsigned int) );
	EstelaSuperFinal=(unsigned char *)malloc( sizeof(unsigned char)  * cols * rows );

	//Booleano asyncrono que dira si la camara debe de grabar o no
	bool *startRecording=NULL; //Booleano que dira si grabamos con la camara o no

	//Pinned Memory, esta memoria es mas eficiente
	//ya que no necesita consultar al cpu para aceder a ella
	startRecording=(bool *)malloc( sizeof(bool) ) ;	
	
	*startRecording = false;

	uchar4 *uc4Tmp;
	i=0;

	//Objeto para los tiempos de telegram
	RecordTime sTimeTelegram=startTiming(0);

	//Objeto para los tiempos de telegram
	RecordTime mainVideoTime=startTiming(0);
	
	
        while(1)
        {
		cap >> frame; // get a new frame from camera
		
		//Convertir  frame e insertar en el buffer
		cvtColor( frame, colorFrame, CV_BGR2RGBA );

		//Tomando los apuntadores a los datos del buffer
		uc4Tmp= (uchar4 *)colorFrame.ptr<unsigned char>(0);
		
		//Reinicia la estela
		memset( estela+(rows*cols*currentEstela), 0, sizeof(unsigned char) * rows * cols );
		
		//Conseguir el total de pixeles cambiados
		*changedPixels=movementDetect( uc4Tmp, background, out, estela, currentEstela, EstelaSuperFinal, UMBRAL, bufferSize, rows, cols );
		
		float movementPercentaje = ( (float)(*changedPixels*100) ) / ( (float)(rows*cols) );
		movementPercentaje > 0.7 ? *startRecording=true:*startRecording=false;
		//printf("%s\n", *startRecording==true ? "Grabar":"No Grabar!");
		//colorearMovement( uc4Tmp, out, estela, numeroEstelas, rows, cols );
		
		//Estela de salida
		Mat salidaColoreada;
		Mat tmp1Coloreada(rows, cols, CV_8UC4, out );
		Mat estelaMatOut(rows, cols, CV_8UC1, EstelaSuperFinal);

		//Regresar el color a formato BGR, lo manipulamos usando RGBA
		cvtColor( tmp1Coloreada, salidaColoreada, CV_RGBA2BGR);
		printf("%s\n", *startRecording == 1 ? "Grabando!":"No Grabar!");
		//if( smartRecord( frame, TeleVideo, VideoVigilancia, *startRecording, &mainVideoTime, &sTimeTelegram) ) printf( "Something went wrong :(\n");

		imshow("Original", frame);
		imshow("Estela", estelaMatOut);
		imshow("Coloreada", salidaColoreada);

		if( waitKey(27) == 27 ) break;

		//Estela actual
		currentEstela = ( currentEstela + 1 ) % numeroEstelas;

		//Reinicia la estela de salida
		memset(EstelaSuperFinal, 0, sizeof(unsigned char) * rows * cols);

	}
	free(out);
	free(startRecording);
	free(changedPixels);
	free(VideoVigilancia);
	free(TeleVideo);
	free(estela);
	free(background);

    return 0;

}


